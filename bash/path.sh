export PATH=/usr/local/bin:$PATH
# Add rbenv to PATH
export PATH="$HOME/.rbenv/bin:$PATH"
# Add GO to PATH
export GOPATH=$PROJECTS/go
export PATH="$GOPATH/bin:$PATH"
# Add Postgres to PATH
export PATH=/usr/local/bin/psql/:$PATH
# Added Heroku Toolbelt to PATH
export PATH="/usr/local/heroku/bin:$PATH"
# Adds NPM bin to PATH
export PATH="~/.npm-packages/bin:$PATH"
# Adds Brew Cellar to path
export PATH="~/.linuxbrew/Cellar:$PATH"
