alias reset="source ~/.bash_profile"

# Package Management
alias install="sudo apt-get install"
alias remove="sudo apt-get remove"
alias clean="sudo apt-get autoremove"
alias update="sudo apt-get update"
alias upgrade="sudo apt-get ugprade"

function install() {
  case $1 in
    sudo apt-get install);;
  esac
}

function remove() {
  case $1 in
    sudo apt-get remove);;
  esac
}

function clean() {
  case $1 in
    sudo apt-get autoremove);;
  esac
}

function update() {
  case $1 in
    sudo apt-get update);;
  esac
}

function upgrade() {
  case $1 in
    sudo apt-get upgrade);;
  esac
}
# Directory
alias .="ls"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

# Aliases
alias c="clear"
alias l='ls -CF'
alias ls='ls --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias grep='grep -rin --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

function clone() {

}
function pull {

}
function push {

}
function up {

}

function zipf () { zip -r "$1".zip "$1" ; }

function extract() {
  if [ -f $1 ]; then
    case $1 in
      *.tar.bz2)  tar xjf $1    ;;
      *.tar.gz)   tar xzf $1    ;;
      *.bz2)    bunzip2 $1    ;;
      *.rar)    unrar e $1    ;;
      *.gz)   gunzip $1   ;;
      *.tar)    tar xf $1   ;;
      *.tbz2)   tar xjf $1    ;;
      *.tgz)    tar xjf $1    ;;
      *.zip)    unzip $1    ;;
      *.Z)    uncompress$1  ;;
      *.7z)   7z x $1     ;;
      *)      echo "'$1' cannot be extracted with extract()"  ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
